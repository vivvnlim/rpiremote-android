package com.mjlim.rpiremote;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.Set;
import java.util.UUID;

import com.mjlim.rpiremote.Messaging;

/**
 * Created by Mike on 2/7/2015.
 */
public class BTRemote {

	public static final String DEVICE_NAME = "mjl-pi2";

	public WeakReference<OnMessageReceivedHandler> receiveHandler;
	public WeakReference<OnConnectionLostHandler> connectionLostHandler;

	private UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee");
	// old uuid 94f39d29-7d6d-437d-973b-fba39e49d4ee
	// new uuid 787d84c0-bfe7-11e4-a04d-0002a5d5c51b

	private BluetoothDevice btDevice;
	private BluetoothSocket btSocket;

	private ListenForMessages listenForMessagesTask;

	public BTRemote()
	{
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

		// TODO: handle !adapter.isEnabled()

		Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
		if (pairedDevices.size() > 0)
		{
			for (BluetoothDevice device : pairedDevices)
			{
				if (device.getName().equals(DEVICE_NAME))
				{
					btDevice = device;
					break;
				}
			}
		}
		try
		{
			btSocket = btDevice.createRfcommSocketToServiceRecord(uuid);
			btSocket.connect();

			listenForMessagesTask = new ListenForMessages(btSocket.getInputStream());
			listenForMessagesTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Log.e("BTRemote", "exception while creating socket: " + e.toString());
		}
	}

	public void Send(Messaging.Message message)
	{
		try
		{
			if (!btSocket.isConnected())
			{
				btSocket.connect();
			}
		}
		catch (IOException e)
		{
			Log.e("BTRemote", "exception while connecting: " + e.toString());
		}
		SendTask sendTask = new SendTask();
		sendTask.execute(message);
	}

	public void Send(Messaging.Command command)
	{
		Messaging.Message message = Messaging.Message.newBuilder()
				.setCommand(command)
				.build();
		Send(message);
	}

	public void Send(Messaging.MenuItems command)
	{
		Messaging.Message message = Messaging.Message.newBuilder()
				.setMenuItems(command)
				.build();
		Send(message);
	}

	public void SendCommand(String action, String... args)
	{
		Messaging.Command.Builder commandBuilder = Messaging.Command.newBuilder().setAction(action);
		for (String arg : args)
		{
			commandBuilder.addArgs(arg);
		}
		Send(commandBuilder.build());
	}

	public interface OnMessageReceivedHandler
	{
		public void OnMessageReceived(Messaging.Message inMessage);
	}

	public interface OnConnectionLostHandler
	{
		public void OnConnectionLostReceived();
	}

	public class SendTask extends AsyncTask<Messaging.Message, Void, Void>
	{
		@Override
		protected Void doInBackground(Messaging.Message... params) {
			for (Messaging.Message message : params)
			{
				try {
					DataOutputStream outStream = new DataOutputStream(btSocket.getOutputStream());
					Log.i("BTRemote", "sending message of size" + message.getSerializedSize());
					outStream.writeInt(message.getSerializedSize());
					message.writeTo(outStream);
				}
				catch (IOException e)
				{
					Log.e("BTRemote", "IOException in send");
				}
			}
			return null;
		}
	}

	public class ListenForMessages extends AsyncTask<Void, Messaging.Message, Void>
	{
		DataInputStream inStream;
		public ListenForMessages(InputStream inStream)
		{
			this.inStream = new DataInputStream(inStream);
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			int msgLength;
			try
			{
				while (true)
				{
					msgLength = inStream.readInt();
					byte[] data = new byte[msgLength];
					inStream.readFully(data);
					Messaging.Message message = Messaging.Message.parseFrom(data);
					publishProgress(message);
				}
			}
			catch (IOException e)
			{
				Log.e("BTRemote", "IOException while reading");
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			if (connectionLostHandler == null) { return; }
			OnConnectionLostHandler handler = connectionLostHandler.get();
			if (handler == null) { return; }
			handler.OnConnectionLostReceived();
		}

		@Override
		protected void onProgressUpdate(Messaging.Message... messages)
		{
			Log.i("BTRemote", "Received message");
			if (receiveHandler == null) { return; }
			OnMessageReceivedHandler handler = receiveHandler.get();
			if (handler == null) { return; }

			for (Messaging.Message message : messages)
			{
				handler.OnMessageReceived(message);
			}
		}
	}

	public void destroy()
	{
		try
		{
			btSocket.close();
		}
		catch (IOException e)
		{
			Log.e("BTRemote", "IOException while closing");
		}
	}
}
