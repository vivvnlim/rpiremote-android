package com.mjlim.rpiremote;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Mike on 3/1/2015.
 */
public class MenuAdapter extends BaseAdapter {
	private Context context;
	private Messaging.MenuItems menuItems;

	public MenuAdapter(Messaging.MenuItems menuItems)
	{
		this.menuItems = menuItems;
	}

	public void updateItems(Messaging.MenuItems menuItems)
	{
		this.menuItems = menuItems;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return menuItems.getItemsCount();
	}

	@Override
	public Object getItem(int position) {
		return menuItems.getItems(position);
	}

	// typed version of getItem
	public Messaging.MenuItems.MenuItem getMenuItem(int position)
	{
		return menuItems.getItems(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View newView = convertView;
		if (newView == null)
		{
			LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			newView = inflater.inflate(R.layout.listitem_cmds, null);
		}

		Messaging.MenuItems.MenuItem item = menuItems.getItems(position);
		TextView label = (TextView)newView.findViewById(R.id.cmdItemLabel);
		if (item.hasLabel())
		{
			label.setText(item.getLabel());
		}
		else
		{
			label.setText("Item " + position);
		}

		return newView;
	}
}
