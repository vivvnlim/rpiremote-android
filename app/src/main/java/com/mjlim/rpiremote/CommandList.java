package com.mjlim.rpiremote;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.ref.WeakReference;


public class CommandList extends ActionBarActivity implements AdapterView.OnItemClickListener, BTRemote.OnMessageReceivedHandler, BTRemote.OnConnectionLostHandler {

	private BTRemote remote;
	private String[] cmds;
	private ListView cmdListView;
	private TextView messageTextView;

	private Vibrator vibrator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_command_list);

		vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);

		cmdListView = (ListView) this.findViewById(R.id.cmdListView);
		cmdListView.setOnItemClickListener(CommandList.this);
		messageTextView = (TextView) this.findViewById(R.id.message);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		new BackgroundConnect().execute();

	}

	@Override
	protected void onDestroy()
	{
		remote.destroy();
		super.onDestroy();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_command_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Object rawItem = parent.getAdapter().getItem(position);
		if (rawItem instanceof Messaging.MenuItems.MenuItem)
		{
			Messaging.MenuItems.MenuItem item = (Messaging.MenuItems.MenuItem) rawItem;

			Messaging.Command cmd = Messaging.Command.newBuilder()
					.setAction(item.getCommandAction())
					.addAllArgs(item.getCommandArgsList())
					.build();
			remote.Send(cmd);
		}
	}

	@Override
	public void OnMessageReceived(Messaging.Message inMessage) {
		if (inMessage.hasCommand())
		{
			Messaging.Command command = inMessage.getCommand();
			if (command.hasAction())
			{
				String action = command.getAction();
				// todo: handle commands from svc
				if (action.equals("fyi") && command.getArgsCount() > 0)
				{
					vibrator.vibrate(500);
					messageTextView.setText(command.getArgs(0));
				}
				else if (action.equals("bigfyi") && command.getArgsCount() > 0)
				{
					vibrator.vibrate(500);
					Log.i("CommandList", "Showing bigfyi");
					Intent i = new Intent(this, ScrollableText.class);
					i.putExtra(ScrollableText.TEXT_TO_DISPLAY_EXTRA, command.getArgs(0));
					startActivity(i);
				}
			}
		}
		if (inMessage.hasMenuItems())
		{
			Messaging.MenuItems menuItems = inMessage.getMenuItems();
			MenuAdapter adapter = new MenuAdapter(menuItems);
			cmdListView.setAdapter(adapter);
		}
	}

	@Override
	public void OnConnectionLostReceived() {
		finish();
	}

	private class BackgroundConnect extends AsyncTask<String, String, Void> {

		@Override
		protected Void doInBackground(String... params) {

			publishProgress("Initializing BTRemote");
			remote = new BTRemote();
			remote.receiveHandler = new WeakReference<BTRemote.OnMessageReceivedHandler>(CommandList.this);
			remote.connectionLostHandler = new WeakReference<BTRemote.OnConnectionLostHandler>(CommandList.this);
			publishProgress("Retrieving Commands");
			try
			{
				remote.SendCommand("get_menu", "main");
				publishProgress("");
				vibrator.vibrate(500);
				return null;
			}
			catch (Exception e)
			{
				publishProgress(e.toString());
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... progress)
		{
			messageTextView.setText(progress[0]);
		}
	}
}
