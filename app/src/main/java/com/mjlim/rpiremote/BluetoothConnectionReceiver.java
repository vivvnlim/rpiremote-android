package com.mjlim.rpiremote;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Mike on 3/4/2015.
 */
public class BluetoothConnectionReceiver extends BroadcastReceiver {

	public BluetoothConnectionReceiver()
	{}

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("BTConRecv", "Received intent with action " + intent.getAction());
		if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(intent.getAction()))
		{
			BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			if (device.getName().equals(BTRemote.DEVICE_NAME))
			{
				Intent i = new Intent(context, CommandList.class);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
			}
		}
	}
}
